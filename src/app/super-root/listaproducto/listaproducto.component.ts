import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {GlobalService} from '../../shared/services/global/global.service';
import {
  SwiperComponent,
  SwiperConfigInterface, SwiperDirective,
} from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-listaproducto',
  templateUrl: './listaproducto.component.html',
  styleUrls: ['./listaproducto.component.scss']
})
export class ListaproductoComponent implements OnInit {
  public productos;
  public view: boolean;
  public show = true;
  public products = [];
  public produto = {
    ref: '',
    may: 0,
    pub: 0
  };
  public id = 0;
  public type = 'component';

  public disabled = false;

  public config: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: true,
    scrollbar: true,
    navigation: true,
    pagination: false
  };
  @ViewChild(SwiperComponent, {read: false}) componentRef?: SwiperComponent;
  @ViewChild(SwiperDirective, {read: false}) directiveRef?: SwiperDirective;

  constructor(private router: ActivatedRoute, private globalService: GlobalService) {
    this.router.params.subscribe(params => {
      const id = params.categoria;
      globalService.getProductos(id).subscribe(productos => {
        this.productos = productos.productos;
      });
    });
  }

  ngOnInit() {
    this.view = sessionStorage.getItem('show') === 'true';
  }

  format(numero) {
    return new Intl.NumberFormat('de-DE').format(numero);
  }

  modal(id) {
    this.id = id;
    let tmp = [];
    this.products = [];
    this.productos.forEach(e => {
      if (e.id === id) {
        this.produto = e;
      }
      if (e.id >= id) {
        this.products.push(e);
      } else {
        tmp.push(e);
      }
    });
    tmp = tmp.reverse();
    tmp.forEach(e => {
      this.products.push(e);
    });
    const button = document.getElementById('modal');
    button.click();
    this.toggleMouseWheelControl();
  }

  public toggleMouseWheelControl(): void {
    this.config.mousewheel = !this.config.mousewheel;
  }

  public onIndexChange(index: number): void {
    this.id = this.products[index].id;
  }

  public onSwiperEvent(event: string): void {
    console.log('Swiper event: ', event);
  }
}
