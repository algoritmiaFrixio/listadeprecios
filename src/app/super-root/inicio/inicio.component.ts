import {Component, OnInit} from '@angular/core';
import {GlobalService} from '../../shared/services/global/global.service';
import {MzModalService, MzToastService} from 'ngx-materialize';
import {Service} from '../../shared/interfaces/global/service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {VentaService} from '../../shared/services/venta/venta.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  public services: Service[];
  public show: boolean;
  public VentaForm: FormGroup;
  public search = '';
  public productos = [];
  public producto = [];
  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '10%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
  };

  constructor(private globalService: GlobalService, private toastService: MzToastService, private ventaService: VentaService, public router: Router, private modalService: MzModalService) {
    this.VentaForm = new FormGroup({
      telefono: new FormControl('', Validators.required),
      nombre: new FormControl('', Validators.required),
      id_user: new FormControl(''),
      id_service: new FormControl(''),
    });
    globalService.getProductos('all').subscribe(productos => {
      this.productos = productos.productos;
    });
  }

  applyFilter(filterValue) {
    setTimeout(() => {
      this.producto = [];
      this.productos.forEach(e => {
        if (e.ref.indexOf(filterValue) > -1) {
          this.producto.push(e);
        }
      });
    });
  }

  public venta(form: FormGroup, id) {
    form.value.id_service = id;
    form.value.id_user = JSON.parse(localStorage.getItem('user')).id;
    this.ventaService.store(form.value).subscribe((response: any) => {
      this.toastService.show(response.message, 4000, 'green');
      this.VentaForm.reset();
    }, error1 => {
      this.toastService.show(error1.error.message, 4000, 'red');
    });
  }

  ngOnInit() {
  }

  public Confirmation(show) {
    this.show = show;
    sessionStorage.setItem('show', show);
  }
  clear() {
    this.applyFilter('');
    this.search = '';
  }

}
