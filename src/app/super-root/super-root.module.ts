import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SuperRootRoutingModule} from './super-root-routing.module';
import {MaterializeModule} from '../materialize/materialize.module';
import {InicioComponent} from './inicio/inicio.component';
import {ListaproductoComponent} from './listaproducto/listaproducto.component';
import {SWIPER_CONFIG, SwiperConfigInterface, SwiperModule} from 'ngx-swiper-wrapper';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  slidesPerView: 1,
  centeredSlides: true
};

@NgModule({
  declarations: [InicioComponent, ListaproductoComponent,],
  imports: [
    SwiperModule,
    CommonModule,
    SuperRootRoutingModule,
    MaterializeModule
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class SuperRootModule {
}
