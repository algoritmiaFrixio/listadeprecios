import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MainComponent} from './main/main.component';
import {SidenavComponent} from './sidenav/sidenav.component';
import {HeaderComponent} from './header/header.component';
import {LogoutComponent} from './logout/logout.component';
import {PortadaComponent} from './portada/portada.component';
import {NgxUiLoaderHttpModule, NgxUiLoaderModule, NgxUiLoaderRouterModule} from 'ngx-ui-loader';
import {MaterializeModule} from './materialize/materialize.module';



@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SidenavComponent,
    HeaderComponent,
    LogoutComponent,
    PortadaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterializeModule,
    NgxUiLoaderModule,
    NgxUiLoaderHttpModule.forRoot({showForeground: true}),
    NgxUiLoaderRouterModule.forRoot({showForeground: true}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
