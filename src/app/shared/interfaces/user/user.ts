export interface User {
    id?: number;
    nombre?: string;
    local?: string;
    telefono?: string;
    telefono_local?: string;
    cedula?: string;
    contrasena?: string;
    ciudad?: string;
    departamento?: string;
    barrio?: string;
    direccion?: string;
    puntos?: string;
    redimidos?: string;
    token?: string;
}
