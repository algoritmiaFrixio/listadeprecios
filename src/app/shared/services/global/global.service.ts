import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(private http: HttpClient) {
  }

  public login(data): Observable<any> {
    return this.http.post<any>(`${environment.urlBase}authFrixionista`, data,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public pedido(data): Observable<any> {
    const tmp = {
      carrito: data,
      frixionista: JSON.parse(localStorage.getItem('user'))
    };
    return this.http.post<any>(`${environment.urltodo}pedido`, tmp,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public getProductos(categoria): Observable<any> {
    return this.http.get(`${environment.urltodo}productos?cat=${categoria}`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public getProductoTalla(): Observable<any> {
    return this.http.get(`${environment.urltodo}tallas`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public getProducto(id): Observable<any> {
    return this.http.get(`${environment.urltodo}producto?id=${id}`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }
}
