import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PuntoService {

  constructor(private http: HttpClient) {
  }

  public index(id): Observable<any> {
    return this.http.get<any>(`${environment.urlBase}point?id=${id}`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }
}
