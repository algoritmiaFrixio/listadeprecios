import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MzButtonModule,
  MzCardModule, MzCollapsibleModule,
  MzInputModule, MzModalModule,
  MzNavbarModule, MzValidationModule,
  MzSidenavModule, MzToastModule,
  MzTooltipModule, MzBadgeModule, MzSelectModule,
} from 'ngx-materialize';
import {ChartsModule} from 'ng2-charts';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material';

const data = [
  CommonModule,
  MzInputModule,
  MzButtonModule,
  MzSidenavModule,
  MzNavbarModule,
  ChartsModule,
  MzTooltipModule,
  MzCardModule,
  HttpClientModule,
  ReactiveFormsModule,
  FormsModule,
  MzToastModule,
  MzModalModule,
  MzCollapsibleModule,
  MzBadgeModule,
  MzSelectModule,
  MzValidationModule,
  MatTableModule
];

@NgModule({
  declarations: [],
  imports: data,
  exports: data
})
export class MaterializeModule {
}
